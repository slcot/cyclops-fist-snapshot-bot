(function(botHandler){

	var BOT_NAME = "Fist Snapshot Bot";

	var exports = {
		onStart: function onStart(){
			(new dialogBox({
				type: dialogBox.OKAY,
				instructions: BOT_NAME + ' is now watching.'
			})).open();
		},

		onGesture: function onGesture(gesture){
			botHandler.takeSnapshot();
		}
	};

	botHandler.loadExportsFor(BOT_NAME, exports);

})(window.botHandler);
